import mongoose from "mongoose";
import requiredEnv from "./requiredEnv.js";

const Image = mongoose.model(
  "images",
  new mongoose.Schema({
    url: { type: String, required: true, unique: true },
  }),
);

class Database {
  async init() {
    await mongoose.connect(requiredEnv("MONGODB_URI"));
  }

  async getRandom() {
    const randomImage = await Image.aggregate<{ url: string }>([{
      $sample: { size: 1 },
    }]).exec();
    return randomImage[0].url;
  }
}
const database = new Database();

export default database;
