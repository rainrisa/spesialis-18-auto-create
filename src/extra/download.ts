import Downloader from "nodejs-file-downloader";

export default async function download(url: string, directory?: string) {
  const downloader = new Downloader({ url, directory });
  const { filePath } = await downloader.download();
  return filePath;
}
