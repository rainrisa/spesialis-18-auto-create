export default function getText<T extends Record<string, any>>(
  template: string,
  data: T
) {
  return template.replace(
    /\{([^}]+)\}/g,
    (match, key) => data[key.trim()] || ""
  );
}
