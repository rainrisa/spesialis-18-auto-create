export interface Chat {
  id: number;
  name: string;
  part: number;
  template: string;
  link: string;
  baseGroupId: number;
}
export interface RegisteredChat {
  [baseGroupId: number]: {
    baseGroupId: number;
    vidGroupIds: number[];
  };
}
export type UpdatedChat = Chat & { newLink: string };
