interface Reaction {
  title: string;
  reaction: string;
}
export default function getEmoji(
  reactions: Reaction[],
  allowedReactions: string[]
) {
  const filteredReactions = reactions.filter((reaction) => {
    return allowedReactions.includes(reaction.title);
  });
  return filteredReactions.map((reaction) => reaction.reaction);
}
