import axios from "axios";
import requiredEnv from "./services/requiredEnv.js";
import { Chat, UpdatedChat } from "./interfaces.js";
import spesialis from "./services/spesialis.js";
import getText from "./extra/getText.js";
import { sleep } from "telegram/Helpers.js";
import redis from "./services/redis.js";
import shuffle from "./extra/shuffle.js";
import database from "./services/database.js";

async function main() {
  await database.init();
  await spesialis.start();

  const apiUrl = `${requiredEnv("API_URL")}/chats`;
  const autopostApiUrl = `${requiredEnv("AUTOPOST_API_URL")}/link`;
  const chats = (await axios.get<Chat[]>(apiUrl)).data;
  const bannedChats = await spesialis.getBannedChats(chats);
  const justCheck = process.argv[2];

  if (justCheck) {
    process.exit();
  }
  if (!bannedChats.length) {
    console.log("There is no banned chat");
    process.exit();
  }
  await redis.init();
  shuffle(bannedChats);

  if (bannedChats.length > 2) {
    bannedChats.length = 2;
  }
  const updatedChats: UpdatedChat[] = [];
  const redisTask: { oldVidGroupId: number; newVidGroupId: number }[] = [];

  for (const chatInfo of bannedChats) {
    await sleep(10000);

    const oldVidGroupId = chatInfo.id;
    const newPart = chatInfo.part + 1;
    const newTitle = getText(chatInfo.template, { ...chatInfo, part: newPart });
    const newChat = await spesialis.createGroup(newTitle);
    const newLink = await spesialis.getInviteLink(newChat);

    await spesialis.setPhoto(newChat);
    await spesialis.join(newLink);
    await spesialis.setReactions(newChat);
    await spesialis.providerAccount.muteChat(newChat);
    await spesialis.setAdmins(newChat);
    await spesialis.configureChat(newChat);
    await spesialis.beAnonymousAdmin(newChat);
    await spesialis.clearChat(newChat);

    const baseGroupId = chatInfo.baseGroupId;
    const vidGroupId = Number(`-100${newChat.id.toJSNumber()}`);

    spesialis.forwardMedia(baseGroupId, vidGroupId);

    updatedChats.push({
      ...chatInfo,
      id: vidGroupId,
      part: newPart,
      newLink,
    });
    redisTask.push({ oldVidGroupId, newVidGroupId: vidGroupId });
    console.log("\n");
  }
  for (const updatedChat of updatedChats) {
    await axios.patch(apiUrl, { chat: updatedChat });
    console.log(`${updatedChat.name} is updated with ${updatedChat.newLink}`);

    axios.post(autopostApiUrl, {
      link: updatedChat.newLink,
      button: { text: "JOIN GRUP", url: updatedChat.newLink },
    });
    console.log(`New chat's link is already sent to the autopost server`);
    await sleep(2000);
  }
  for (const task of redisTask) {
    await spesialis.updateRedisChats(task.oldVidGroupId, task.newVidGroupId);
  }
  await spesialis.providerAccount.sendMessage(
    "JennyRbyJnKim_Bot",
    "/refresh",
  );
  await spesialis.refreshChatList(apiUrl);
}
main();
