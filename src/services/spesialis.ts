import bigInt from "big-integer";
import { EntityLike } from "telegram/define.js";
import requiredEnv from "./requiredEnv.js";
import { Telegram } from "./telegram.js";
import { Api } from "telegram";
import { Chat } from "../interfaces.js";
import getEmoji from "../extra/getEmoji.js";
import { escapeRegex, sleep } from "telegram/Helpers.js";
import shuffle from "../extra/shuffle.js";
import axios from "axios";
import getText from "../extra/getText.js";
import redis from "./redis.js";
import database from "./database.js";
import download from "../extra/download.js";

const providerSession = requiredEnv("PROVIDER_SESSION");
const clientSession = requiredEnv("CLIENT_SESSION");
const descriptionText = requiredEnv("DESCRIPTION_TEXT");

export class Spesialis {
  providerAccount: Telegram;
  clientAccount: Telegram;

  constructor() {
    this.providerAccount = new Telegram(providerSession);
    this.clientAccount = new Telegram(clientSession);
  }

  async start() {
    await this.providerAccount.start();
    await this.clientAccount.start();
  }

  async getBannedChats(chats: Chat[]) {
    const dialogs = await this.clientAccount.client.getDialogs();
    const bannedChats: Chat[] = [];

    for (const chat of chats) {
      const chatId = bigInt(chat.id);
      const entity = dialogs.find((d) => d.id?.equals(chatId))?.entity;

      if (entity) {
        const banned = await spesialis.isBanned(entity);

        if (banned) {
          console.log(chat.name, "is banned!");
          bannedChats.push(chat);
        }
      }
    }

    return bannedChats;
  }

  async isBanned(entity: EntityLike) {
    const chat = (await this.clientAccount.client.getEntity(
      entity,
    )) as Api.Channel;
    return Boolean(chat.restricted);
  }

  async createGroup(title: string, description?: string) {
    console.log("Creating", title);
    if (!description) description = descriptionText;
    return this.providerAccount.createGroup(title, description);
  }

  async setPhoto(channel: EntityLike) {
    const imageUrl = await database.getRandom();
    const imagePath = await download(imageUrl, "./images");

    if (!imagePath) {
      throw Error(`image ${imageUrl} is null`);
    }
    console.log("Set channel profile photo");
    return this.providerAccount.setPhoto(channel, imagePath);
  }

  async join(inviteLink: string) {
    console.log("Joining", inviteLink);

    const result = (await this.clientAccount.joinChat(
      inviteLink,
    )) as Api.Updates;
    const chat = result.chats[0] as Api.Channel;

    await this.clientAccount.muteChat(chat);
  }

  async setReactions(entity: EntityLike) {
    const availableReactions = await this.providerAccount
      .getAvailableReactions();

    if (availableReactions) {
      const newReactions = getEmoji(availableReactions, [
        "Red Heart",
        "Fire",
        "Smiling Face with Hearts",
      ]);
      await this.providerAccount.setDefaultReactions(entity, newReactions);
      console.log("Successfully set new reactions:", newReactions);
    }
  }

  async setAdmins(entity: EntityLike) {
    const botUsernames = requiredEnv("BOT_USERNAMES").split(" ");

    for (const botUsername of botUsernames) {
      await this.providerAccount.addAdmin(entity, botUsername);
      console.log(`Successfully added ${botUsername} as admin`);
    }
  }

  async configureChat(entity: EntityLike) {
    const messages = [
      "/welcome off",
      "/goodbye off",
      "/cleanservice all",
      "/lock all",
    ];
    await sleep(1000);

    for (const message of messages) {
      await this.providerAccount.sendMessage(entity, message);
      await sleep(2000);
    }
  }

  async beAnonymousAdmin(entity: EntityLike) {
    const providerId = (await this.providerAccount.getMe()).id;
    const oldAdminRights = await this.providerAccount.getAdminPermissions(
      entity,
    );
    const newAdminRights = { anonymous: true };

    await this.providerAccount.setAdminPermissions(
      entity,
      providerId,
      oldAdminRights,
      newAdminRights,
    );
    console.log(`Successfully set our account to become anonymous`);
  }

  async clearChat(entity: EntityLike) {
    await this.providerAccount.clearChat(entity);
  }

  async forwardMedia(baseGroupId: number, vidGroupId: number) {
    console.log(`Forwards from ${baseGroupId} to ${vidGroupId}`);

    const messageIds = await this.clientAccount.getVideoMessageIds(baseGroupId);
    const forwardDelay = 3000;
    const botToken = requiredEnv("FORWARD_BOT_TOKEN");
    const requestUrl = `https://api.telegram.org/bot${botToken}/copyMessage`;

    shuffle(messageIds);

    for (const messageId of messageIds) {
      await sleep(forwardDelay);

      try {
        await axios.get(requestUrl, {
          data: {
            chat_id: vidGroupId,
            from_chat_id: baseGroupId,
            message_id: messageId,
            caption: "",
            disable_notification: true,
            protect_content: true,
          },
        });
      } catch (err) {
        console.log((err as Error).message);
      }
    }
    console.log(`Finished from ${baseGroupId} to ${vidGroupId}`);
    await this.setPinned(vidGroupId);
  }

  async getInviteLink(entity: EntityLike) {
    return this.providerAccount.getInviteLink(entity);
  }

  async refreshChatList(apiUrl: string) {
    const chats = (await axios.get<Chat[]>(apiUrl)).data;
    const channelUsername = requiredEnv("BASE_CHANNEL_USERNAME");
    const messageId = Number(requiredEnv("BASE_MESSAGE_ID"));
    const text = await this.clientAccount.getMessageText(
      channelUsername,
      messageId,
    );
    const arrText = text.split("\n");
    const objArrText = arrText.map((r) => {
      const labelAndLink = r.split(": ");
      return { label: labelAndLink[0], link: labelAndLink[1] };
    });
    objArrText.forEach((text) => {
      chats.forEach((newChat) => {
        const oldChatTitle = getText(newChat.template, {
          ...newChat,
          part: newChat.part - 1,
        });
        if (text.label.match(escapeRegex(oldChatTitle))) {
          const newChatTitle = getText(newChat.template, newChat);
          console.log("Refresh chat", newChatTitle);

          text.label = newChatTitle;
          text.link = newChat.link;
        }
      });
    });
    const newText = objArrText
      .map((arrText) => {
        return `${arrText.label}: ${arrText.link}`;
      })
      .join("\n");

    return this.providerAccount.editText(channelUsername, messageId, newText);
  }

  async setPinned(entity: EntityLike) {
    const fromChat = "berbagi_link_bokep";
    const forwardMessageId = 99;
    const messages = await this.providerAccount.forwardMessage(
      fromChat,
      forwardMessageId,
      entity,
    );
    return this.providerAccount.pinMessage(entity, messages[0][0].id);
  }

  async updateRedisChats(oldVidGroupId: number, newVidGroupId: number) {
    const redisBaseGroupId = redis.getBaseGroup(oldVidGroupId) || 1;
    const redisVidGroupIds = redis.getVidGroups(redisBaseGroupId);
    const vidGroupIds = redisVidGroupIds.filter(
      (id) => id !== oldVidGroupId,
    );
    vidGroupIds.push(newVidGroupId);
    await redis.update(redisBaseGroupId, vidGroupIds);
  }
}
const spesialis = new Spesialis();

export default spesialis;
