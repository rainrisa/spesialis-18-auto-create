import BaseRedis from "ioredis";
import { RegisteredChat } from "../interfaces.js";
import requiredEnv from "./requiredEnv.js";

export class Redis {
  client: BaseRedis;
  data: RegisteredChat;

  constructor(uri: string) {
    this.client = new BaseRedis(uri);
    this.data = {};
  }

  async init() {
    const configChatKeys = await this.client.keys(`config/chat/*`);
    const baseGroupIds = configChatKeys.map((key) => Number(key.split("/")[2]));

    for (const baseGroupId of baseGroupIds) {
      const response = await this.client.get(`config/chat/${baseGroupId}`);

      if (response) {
        const vidGroupIds: number[] = JSON.parse(response);
        this.data[baseGroupId] = { baseGroupId, vidGroupIds };
      }
    }
  }

  getBaseGroup(vidGroupId: number) {
    const baseGroupIds = Object.keys(this.data).map(Number);

    return baseGroupIds.find((baseGroupId) =>
      this.data[baseGroupId].vidGroupIds.includes(vidGroupId)
    );
  }

  getVidGroups(baseGroupId: number) {
    const chat = this.data[baseGroupId];
    return chat ? chat.vidGroupIds : [];
  }

  async update(baseGroupId: number, vidGroupIds: number[]) {
    await this.client.set(
      `config/chat/${baseGroupId}`,
      JSON.stringify(vidGroupIds),
    );
    this.data[baseGroupId] = {
      baseGroupId,
      vidGroupIds,
    };
    console.log(`redis ${baseGroupId} is updated with ${String(vidGroupIds)}`);
  }
}
const redis = new Redis(requiredEnv("REDIS_URI"));

export default redis;
