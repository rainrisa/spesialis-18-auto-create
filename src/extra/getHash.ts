export default function getHash(inviteLink: string) {
  return inviteLink.replace("https://t.me/+", "");
}
