import { basename as getFileName } from "node:path";
import { statSync } from "node:fs";
import { Api, TelegramClient } from "telegram";
import { StringSession } from "telegram/sessions/index.js";
import { Entity, EntityLike } from "telegram/define.js";
import getHash from "../extra/getHash.js";
import { TotalList } from "telegram/Helpers.js";
import { Dialog } from "telegram/tl/custom/dialog.js";
import { CustomFile } from "telegram/client/uploads.js";

export class Telegram {
  client: TelegramClient;
  dialogs: TotalList<Dialog>;

  constructor(session: string) {
    this.client = new TelegramClient(
      new StringSession(session),
      611335,
      "d524b414d21f4d37f08684c1df41ac9c",
      {
        connectionRetries: 5,
      },
    );
    this.dialogs = [];
  }

  async start() {
    await this.client.start({
      phoneNumber: async () => "",
      password: async () => "",
      phoneCode: async () => "",
      onError: (err) => console.log(err),
    });
    const me = (await this.client.getMe()) as Api.User;
    this.dialogs = await this.client.getDialogs();

    console.log(`Logged in as ${me.firstName}`);
  }

  async getMe() {
    return (await this.client.getMe()) as Api.User;
  }

  async sendMessage(entity: EntityLike, text: string) {
    return this.client.sendMessage(entity, {
      message: text,
      linkPreview: false,
    });
  }

  async createGroup(title: string, description?: string) {
    const result = await this.client.invoke(
      new Api.channels.CreateChannel({
        title,
        about: description || "",
        megagroup: true,
      }),
    );
    if (result.className === "Updates") {
      return result.chats[0] as Api.Channel;
    }
    throw Error(`Cannot create supergroup\n${JSON.stringify(result)}`);
  }

  async setPhoto(channel: EntityLike, imagePath: string) {
    const fileName = getFileName(imagePath);
    const fileSize = statSync(imagePath).size;

    const result = await this.client.invoke(
      new Api.channels.EditPhoto({
        channel,
        photo: new Api.InputChatUploadedPhoto({
          file: await this.client.uploadFile({
            file: new CustomFile(fileName, fileSize, imagePath),
            workers: 1,
          }),
        }),
      }),
    );
    return result;
  }

  async addAdmin(entity: EntityLike, user: EntityLike) {
    return this.client.invoke(
      new Api.channels.EditAdmin({
        channel: entity,
        userId: user,
        adminRights: new Api.ChatAdminRights({
          changeInfo: true,
          postMessages: true,
          editMessages: true,
          deleteMessages: true,
          banUsers: true,
          inviteUsers: true,
          pinMessages: true,
          manageCall: true,
        }),
        rank: "",
      }),
    );
  }

  async getInviteLink(entity: EntityLike) {
    const result = await this.client.invoke(
      new Api.messages.GetExportedChatInvites({
        peer: entity,
        adminId: new Api.InputPeerSelf(),
      }),
    );
    const invite = result.invites[0];

    if (invite.className === "ChatInviteExported") {
      return invite.link;
    }
    throw Error(`Cannot get invite link\n${JSON.stringify(result)}`);
  }

  async getAvailableReactions() {
    const result = await this.client.invoke(
      new Api.messages.GetAvailableReactions({
        hash: 0,
      }),
    );
    if (result.className === "messages.AvailableReactions") {
      return result.reactions.map((_reaction) => {
        const { title, reaction } = _reaction;
        return { title, reaction };
      });
    }
  }

  async setDefaultReactions(entity: EntityLike, reactions: string[]) {
    const newReactions = reactions.map((reaction) => {
      return new Api.ReactionEmoji({
        emoticon: reaction,
      });
    });
    return this.client.invoke(
      new Api.messages.SetChatAvailableReactions({
        peer: entity,
        availableReactions: new Api.ChatReactionsSome({
          reactions: newReactions,
        }),
      }),
    );
  }

  async getAdminPermissions(entity: EntityLike) {
    const chatInfo = await this.client.invoke(
      new Api.channels.GetChannels({ id: [entity] }),
    );
    const theChat = chatInfo.chats[0] as Api.Channel;

    return theChat.adminRights as Api.ChatAdminRights;
  }

  async setAdminPermissions(
    chat: EntityLike,
    user: EntityLike,
    oldAdminRights: Api.ChatAdminRights,
    newAdminRights: Partial<Api.ChatAdminRights>,
  ) {
    return this.client.invoke(
      new Api.channels.EditAdmin({
        channel: chat,
        userId: user,
        adminRights: new Api.ChatAdminRights({
          ...oldAdminRights,
          ...newAdminRights,
        }),
        rank: "",
      }),
    );
  }

  async joinChat(inviteLink: string) {
    return this.client.invoke(
      new Api.messages.ImportChatInvite({ hash: getHash(inviteLink) }),
    );
  }

  async muteChat(chat: Api.Channel) {
    const { id: channelId, accessHash } = chat;

    if (!accessHash) {
      throw Error(`accessHash is empty!`);
    }
    const inputPeerChannel = new Api.InputPeerChannel({
      channelId,
      accessHash,
    });
    const inputNotifyPeer = new Api.InputNotifyPeer({
      peer: inputPeerChannel,
    });
    return this.client.invoke(
      new Api.account.UpdateNotifySettings({
        peer: inputNotifyPeer,
        settings: new Api.InputPeerNotifySettings({
          silent: true,
          muteUntil: 2 ** 31 - 1,
        }),
      }),
    );
  }

  async clearChat(chat: EntityLike) {
    return this.client.invoke(
      new Api.channels.DeleteHistory({
        channel: chat,
        forEveryone: true,
      }),
    );
  }

  async getVideoMessageIds(entity: EntityLike) {
    const messageIds: number[] = [];

    for await (
      const message of this.client.iterMessages(entity, {
        reverse: true,
        filter: new Api.InputMessagesFilterVideo(),
      })
    ) {
      const currentMessageId = message.id;
      messageIds.push(currentMessageId);
    }

    return messageIds;
  }

  async getMessageText(entity: EntityLike, messageId: number) {
    const message = (await this.client.invoke(
      new Api.channels.GetMessages({
        channel: entity,
        id: [new Api.InputMessageID({ id: messageId })],
      }),
    )) as Api.messages.ChannelMessages;

    return (message.messages[0] as Api.Message).message;
  }

  async editText(entitiy: EntityLike, messageId: number, newText: string) {
    return this.client.invoke(
      new Api.messages.EditMessage({
        peer: entitiy,
        id: messageId,
        message: newText,
        noWebpage: true,
      }),
    );
  }

  async forwardMessage(
    fromEntity: EntityLike,
    messageId: number,
    toEntity: EntityLike,
  ): Promise<Api.Message[][]> {
    return this.client.forwardMessages(toEntity, {
      messages: messageId,
      fromPeer: fromEntity,
    }) as any;
  }

  async pinMessage(entitiy: EntityLike, messageId: number) {
    return this.client.invoke(
      new Api.messages.UpdatePinnedMessage({
        unpin: false,
        peer: entitiy,
        id: messageId,
      }),
    );
  }
}
